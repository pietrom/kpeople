package com.darwinsw.kpeople.persistence.mongodb

data class MongoDbConfiguration(val connectionString: String, val dbName: String)