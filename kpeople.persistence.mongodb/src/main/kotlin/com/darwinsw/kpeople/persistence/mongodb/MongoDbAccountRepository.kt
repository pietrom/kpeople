package com.darwinsw.kpeople.persistence.mongodb

import com.darwinsw.kpeople.core.Account
import com.darwinsw.kpeople.core.AccountId
import com.darwinsw.kpeople.core.AccountRepository
import com.mongodb.reactivestreams.client.MongoDatabase
import org.bson.codecs.pojo.annotations.BsonId
import org.litote.kmongo.coroutine.CoroutineCollection
import org.litote.kmongo.coroutine.coroutine
import org.litote.kmongo.reactivestreams.KMongo
import org.litote.kmongo.reactivestreams.getCollection

class MongoDbAccountRepository(private val db: MongoDbDatabase) : AccountRepository {
    private val collection : CoroutineCollection<PersistentAccount> = db.getCollection("account");
    override suspend fun save(account: Account) {
        collection.insertOne(PersistentAccount(account.id.value, account.username.value, account.email.value, account.password.value))
    }
}

private data class PersistentAccount(@BsonId val id: String, val username: String, val email: String, val password: String)

