package com.darwinsw.kpeople.persistence.mongodb

import org.litote.kmongo.coroutine.CoroutineCollection
import org.litote.kmongo.coroutine.coroutine
import org.litote.kmongo.reactivestreams.KMongo

class MongoDbDatabase(private val config: MongoDbConfiguration) {
    private val client = KMongo.createClient(config.connectionString).coroutine //use coroutine extension
    private val db = client.getDatabase(config.dbName) //normal java driver usage

    internal inline fun <reified T : Any> getCollection(collectionName: String) : CoroutineCollection<T> = db.getCollection<T>(collectionName);
}