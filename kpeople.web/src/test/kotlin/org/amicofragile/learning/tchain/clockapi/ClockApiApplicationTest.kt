package org.amicofragile.learning.tchain.clockapi

import io.micrometer.prometheus.PrometheusConfig
import io.micrometer.prometheus.PrometheusMeterRegistry
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.junit4.SpringRunner

@RunWith(SpringRunner::class)
@SpringBootTest
@ContextConfiguration(classes = [TestConfiguration::class])
class ClockApiApplicationTest {
	@Test
	fun contextLoads() {
	}

}

@Configuration
open class TestConfiguration {
	@Bean
	open fun meterRegistry(): PrometheusMeterRegistry {
		return PrometheusMeterRegistry(PrometheusConfig.DEFAULT)
	}
}
