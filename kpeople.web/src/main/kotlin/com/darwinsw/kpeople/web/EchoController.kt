package com.darwinsw.kpeople.web

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import java.util.*
import kotlin.math.floor
import kotlin.math.roundToLong

@RestController
@RequestMapping("api/echo")
class EchoController {
    private val randomizer: Random = Random()

    @RequestMapping(method = [RequestMethod.GET])
    suspend fun echo(@RequestParam msg: String): EchoResponse {
        myDelay(1000)
        return EchoResponse(msg)
    }

    private suspend fun myDelay(length: Long) = withContext(Dispatchers.IO) {
        val delay = floor(randomizer.nextDouble() * 1500).roundToLong()
        println(delay)
        Thread.sleep(delay)
        println("Done")
    }
}

data class EchoResponse(val text: String)