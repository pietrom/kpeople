package com.darwinsw.kpeople.web

import com.darwinsw.kpeople.core.AccountService
import org.springframework.http.HttpHeaders
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import java.net.URI
import java.util.*

@RestController
@RequestMapping("api/account")
class AccountController(private val service: AccountService) {
    @RequestMapping(method = [RequestMethod.GET])
    suspend fun echo(): ResponseEntity<Any> {
        val id = service.create()
        return ResponseEntity.created(URI("/account/${id.value}")).build()
    }
}
