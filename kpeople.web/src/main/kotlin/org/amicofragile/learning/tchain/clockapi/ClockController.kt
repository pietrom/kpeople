package org.amicofragile.learning.tchain.clockapi

import io.micrometer.prometheus.PrometheusMeterRegistry
import io.prometheus.client.Counter
import io.prometheus.client.Summary
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod
import org.springframework.web.bind.annotation.RestController
import java.time.ZonedDateTime


@RestController
@RequestMapping("api/clock")
class ClockController(private val registry: PrometheusMeterRegistry, private val clock: Clock) {
    val counter : Counter = Counter.build().name("clock_requests_total").help("Total clock requests.").register(registry.prometheusRegistry)
    val timer : Summary = Summary.build().name("clock_request_summary").help("Clock requests summary").register(registry.prometheusRegistry)

    @RequestMapping(method = [RequestMethod.GET])
    fun now(): ClockStatus {
        val t = timer.startTimer()
        counter.inc()
        try {
            return ClockStatus(clock.now())
        } finally {
            t.observeDuration()
        }
    }
}

data class ClockStatus(val now: ZonedDateTime)

interface Clock {
    fun now(): ZonedDateTime
}

class SystemClock : Clock {
    override fun now(): ZonedDateTime = ZonedDateTime.now()

}