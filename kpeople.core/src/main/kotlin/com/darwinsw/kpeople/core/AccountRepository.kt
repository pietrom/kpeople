package com.darwinsw.kpeople.core

interface AccountRepository {
    suspend fun save(account: Account)
}