package com.darwinsw.kpeople.core

data class Username(val value: String)