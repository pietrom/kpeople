package com.darwinsw.kpeople.core

data class EmailAddress(val value: String) {
    init {
        if(!value.contains('@')) {
            throw IllegalArgumentException()
        }
    }
}