package com.darwinsw.kpeople.core

data class AccountId(val value: String)

class Account internal constructor(
        val id: AccountId,
        val username: Username,
        pazzword: Password,
        val email: EmailAddress) {
    var password: Password = pazzword
        private set(pazzword) {
            field = pazzword
        }

    fun changePassword(old: Password, new: Password) {
        if(password != old) {
            throw IllegalArgumentException()
        }
        password = new;
    }
}