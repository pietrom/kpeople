package com.darwinsw.kpeople.core

data class Password(val value: String) {
    init {
        if(value.length < 6) {
            throw IllegalArgumentException()
        }
    }
}