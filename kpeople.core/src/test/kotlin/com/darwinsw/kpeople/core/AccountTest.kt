package com.darwinsw.kpeople.core

import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Test

class AccountTest {
    companion object {
        val username = Username("pietrom")
        val accountId = AccountId("test-id")
        val email = EmailAddress("pietrom@something.com")
    }
    @Test
    fun canChangePasswordProvidingTheOldOne() {
        val original = Password("abc123")
        val desired = Password("987zyx")
        val account = Account(AccountId("test"), username, original, email)
        account.changePassword(original, desired)
        assertThat(account.password, `is`(desired))
    }

    @Test(expected = java.lang.IllegalArgumentException::class)
    fun cantChangePasswordProvidingTheWrongOne() {
        val original = Password("abc123")
        val desired = Password("987zyx")
        val account = Account(AccountId("test"), username, original, email)
        account.changePassword(Password("thewrongone"), desired)
    }
}