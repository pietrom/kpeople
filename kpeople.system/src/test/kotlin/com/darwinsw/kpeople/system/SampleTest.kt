package com.darwinsw.kpeople.system
import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Test

class SampleTest {
  @Test
  fun sample() {
    assertThat(Sample().text, `is`("sample"))
  }
}

