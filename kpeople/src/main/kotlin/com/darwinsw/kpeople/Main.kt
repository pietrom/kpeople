package com.darwinsw.kpeople

import com.darwinsw.kpeople.core.AccountRepository
import com.darwinsw.kpeople.core.AccountService
import com.darwinsw.kpeople.persistence.mongodb.MongoDbAccountRepository
import com.darwinsw.kpeople.persistence.mongodb.MongoDbConfiguration
import com.darwinsw.kpeople.persistence.mongodb.MongoDbDatabase
import com.darwinsw.kpeople.web.EchoController
import org.amicofragile.learning.tchain.clockapi.Clock
import org.amicofragile.learning.tchain.clockapi.HomeController
import org.amicofragile.learning.tchain.clockapi.SystemClock
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.ComponentScan

@SpringBootApplication
@ComponentScan(basePackageClasses = [HomeController::class, EchoController::class])
open class ClockApiApplication {
    @Bean
    open fun clock() : Clock = SystemClock()
    @Bean
    open fun accountService(repo: AccountRepository) : AccountService = AccountService(repo)
    @Bean
    open fun accountRepository(db: MongoDbDatabase) : AccountRepository = MongoDbAccountRepository(db)
    @Bean
    open fun mongoDbDatabase() : MongoDbDatabase = MongoDbDatabase(MongoDbConfiguration("mongodb://localhost/kpeople", "kpeople"))
}

fun main(args: Array<String>) {
    runApplication<ClockApiApplication>(*args)
}
